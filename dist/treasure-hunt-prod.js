// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"treasure-hunt-prod.js":[function(require,module,exports) {
window.th_init = function (settings) {
  var th_css = document.createElement('link');
  th_css.id = 'th_css';
  th_css.rel = 'stylesheet';
  th_css.href = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/th-style.css';
  document.head.appendChild(th_css);
  var defaultSettings = {
    enable: true,
    blankLootSrc: "https://sg-project-assets.s3.eu-west-2.amazonaws.com/100119/th-badges/blankBadge.svg",
    hideAllLoots: false,
    passportFloat: false,
    progressPassportId: '',
    progressPassportClass: '',
    completeModalClass: '',
    completeModalHtml: "<p>You've found all treasures!!</p>"
  };

  if (settings) {
    for (var key in settings) {
      if (typeof settings[key] != 'undefined') {
        defaultSettings[key] = settings[key];
      }
    }
  } // get loots ready


  var loots = [];
  settings.loots.forEach(function (loot) {
    var temp = {
      id: loot.id,
      path: loot.path,
      point: 1,
      description: loot.desc,
      imgSrc: loot.src,
      videoGo: loot.videoGo,
      item: {
        classes: ["th-item"],
        html: "<img data-th-od=\"".concat(loot.videoGo, "\" src=\"").concat(loot.src, "\" data-th-id=\"").concat(loot.id, "\" />")
      },
      modal: {
        classes: ["th-modal"],
        html: function html() {
          return modalProgressHTML(true);
        }
      }
    };
    loots.push(temp);
  });
  var blankLootSrc = defaultSettings.blankLootSrc;
  var progressPassportId = defaultSettings.progressPassportId;
  addScript('treasure-hunt-func', 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-new-platform.js').then(function () {
    var trackingUrl = '/' + window.location.pathname.split("/")[1];
    treasureHunt({
      huntId: "sg-treasurehunt",
      completedHash: "completed",
      removeHuntOnComplete: false,
      progress: {
        classes: ["th-modal"],
        html: function html(_ref) {
          var loots = _ref.loots;
          return loots.map(function (_ref2) {
            var description = _ref2.description,
                hunted = _ref2.hunted;
            return "";
          }).join("") + modalProgressHTML(defaultSettings.passportFloat);
        }
      },
      loots: loots,
      onComplete: function onComplete() {
        return treasureHuntOncomplete(defaultSettings.completeModalHtml);
      },
      trackingUrl: trackingUrl
    });

    if (progressPassportId != '') {
      initThPassportTab();
      $('.th-item').on('click', function () {
        initThPassportTab();
      });
    }

    ;
    $(document).ready(function () {
      $('.th-item').each(function () {
        var item = $(this);
        var videoGoDuration = parseInt($(this).find('[data-th-id]').attr('data-th-od'));

        if (videoGoDuration != 0) {
          item.hide();
          var hiddenItem = $("<div class=\"th-item-unknown\">\n                    <img src=\"".concat(blankLootSrc, "\" />\n                    </div>")).appendTo($('body'));
          videoGo.treasureHunt(videoGoDuration, showTreasure);
        }

        ;

        if (defaultSettings.hideAllLoots) {
          item.hide();
        }
      });
    });

    function showTreasure() {
      $('.th-item-unknown').hide();
      var thItem = document.getElementsByClassName("th-item");
      var i;

      for (i = 0; i < thItem.length; i++) {
        thItem[i].style.visibility = 'visible';
      }
    }

    ;
  });

  function addScript(scriptName, src) {
    var jsFile = document.createElement('script');
    jsFile.id = scriptName;
    jsFile.src = src;
    document.head.appendChild(jsFile);
    return new Promise(function (res) {
      jsFile.onload = function () {
        res();
      };
    });
  }

  function modalProgressHTML(passportFloat) {
    var lootsProgress = JSON.parse(localStorage.getItem("sg-treasurehunt"));
    var huntedIds = lootsProgress.huntedIds;
    var badgeArray = [];

    for (var i = 0; i < loots.length; i++) {
      var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankLootSrc;
      badgeArray.push("<img src=\"".concat(badgeSrc, "\" width=\"48px\" />"));
    }

    var closeBtn = passportFloat ? "<div id=\"th-close-btn\" class='th-close-btn material-icons-outlined'>close</div>" : "";
    var badgeArray = badgeArray.join("");
    var temp = "<div class=\"th-hunt-progress-content\">\n          ".concat(closeBtn, "\n          <b>Badges Collected</b>\n          <div class=\"badges-array\">\n            ").concat(badgeArray, "\n          </div>\n        </div>");
    return temp;
  }

  function treasureHuntOncomplete(html) {
    var modal = document.createElement("div");
    var overlay = document.createElement("div");
    var pp = document.createElement("div");
    modal.id = "th-complete-modal";
    modal.classList.add("th-modal");
    overlay.classList.add("th-modal-overlay");
    pp.innerHTML = html;
    document.body.appendChild(modal);
    $("#th-complete-modal").append(pp);
    $("#th-complete-modal").after(overlay);
    $("#th-complete-modal img.complete-img").on('load', function () {
      $(this).css({
        'opacity': '1',
        'transform': 'scale(1)'
      });
    });

    overlay.onclick = function () {
      var modal = document.getElementById("th-complete-modal");
      modal.remove();
      $(this).remove();
    };
  }

  ;

  function initThPassportTab() {
    var lootsProgress = JSON.parse(localStorage.getItem("sg-treasurehunt"));
    var huntedIds = JSON.parse(localStorage.getItem("sg-treasurehunt")) ? lootsProgress.huntedIds : [];
    var badgeArray = [];

    for (var i = 0; i < loots.length; i++) {
      var blankBadge = blankLootSrc;
      var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankBadge;
      badgeArray.push("<img src=\"".concat(badgeSrc, "\" width=\"48px\" />"));
    }

    var badgeArray = badgeArray.join("");
    var temp = "<div class=\"th-hunt-progress-content\">\n          <div class=\"badges-array\">\n            ".concat(badgeArray, "\n          </div>\n        </div>");
    $("#".concat(progressPassportId)).html(temp);
  }

  ;
};
},{}],"../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "59448" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../.config/yarn/global/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","treasure-hunt-prod.js"], null)
//# sourceMappingURL=/treasure-hunt-prod.js.map