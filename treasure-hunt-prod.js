window.th_init = (settings) => {
    var th_css = document.createElement('link');
    th_css.id = 'th_css';
    th_css.rel = 'stylesheet';
    th_css.href = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/th-style.css';
    document.head.appendChild(th_css);

    var defaultSettings = {
        enable: true,
        blankLootSrc: `https://sg-project-assets.s3.eu-west-2.amazonaws.com/100119/th-badges/blankBadge.svg`,
        hideAllLoots: false,
        passportFloat: false,
        progressPassportId: '',
        progressPassportClass: '',
        completeModalClass: '',
        completeModalHtml: `<p>You've found all treasures!!</p>`,
    }; 
    if(settings){
        for(const key in settings){
            if(typeof settings[key] != 'undefined'){
                defaultSettings[key] = settings[key]
            }
        }
    }
    
    // get loots ready
    var loots = [];
    settings.loots.forEach(loot => {
        var temp = {
            id: loot.id,
            path: loot.path,
            point: 1,
            description: loot.desc,
            imgSrc: loot.src,
            videoGo: loot.videoGo,
            item: {
                classes: ["th-item"],
                html: `<img data-th-od="${loot.videoGo}" src="${loot.src}" data-th-id="${loot.id}" />`
            },
            modal: {
                classes: ["th-modal"],
                html: () => modalProgressHTML(true)
            } 
        }
        loots.push(temp);
    });

    var blankLootSrc = defaultSettings.blankLootSrc;
    var progressPassportId = defaultSettings.progressPassportId;

    addScript('treasure-hunt-func', 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-new-platform.js')
    .then(() => {
        var trackingUrl = '/'+ window.location.pathname.split("/")[1];
        treasureHunt({
            huntId: "sg-treasurehunt",
            completedHash: "completed",
            removeHuntOnComplete: false,
            progress: {
              classes: ["th-modal"],
              html: ({ loots }) => loots.map(
                  ({ description, hunted }) => ``
                )
                .join("")+modalProgressHTML(defaultSettings.passportFloat)
            },
            loots: loots,
            onComplete: () => treasureHuntOncomplete(defaultSettings.completeModalHtml),
            trackingUrl: trackingUrl
        });

        if(progressPassportId != ''){
            initThPassportTab();
            $('.th-item').on('click', function(){
                initThPassportTab();
            });
        };

        $(document).ready(function(){
            $('.th-item').each(function(){
                var item = $(this);
                var videoGoDuration = parseInt($(this).find('[data-th-id]').attr('data-th-od'));
                if(videoGoDuration != 0){
                    item.hide();
                    var hiddenItem = $(`<div class="th-item-unknown">
                    <img src="${blankLootSrc}" />
                    </div>`).appendTo($('body'));
                    videoGo.treasureHunt(videoGoDuration, showTreasure);
                };

                if(defaultSettings.hideAllLoots){
                    item.hide();
                }
            });
        });

        function showTreasure(){
            $('.th-item-unknown').hide();
            var thItem = document.getElementsByClassName("th-item");
            var i;
            for (i = 0; i < thItem.length; i++) {
                thItem[i].style.visibility = 'visible';
            }
        };
    });

    function addScript(scriptName, src){
        var jsFile = document.createElement('script');
        jsFile.id = scriptName;
        jsFile.src = src;
        document.head.appendChild(jsFile);
    
        return new Promise((res) =>{
            jsFile.onload = function() {
                res();
            }
        })
    }

    function modalProgressHTML(passportFloat){
        var lootsProgress = JSON.parse(localStorage.getItem("sg-treasurehunt"));
        var huntedIds = lootsProgress.huntedIds;
        var badgeArray = [];
        for(var i = 0; i < loots.length; i++){
          var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankLootSrc;
          badgeArray.push(`<img src="${badgeSrc}" width="48px" />`);
        }
        var closeBtn = passportFloat ? `<div id="th-close-btn" class='th-close-btn material-icons-outlined'>close</div>` : ``;
        var badgeArray = badgeArray.join("");
        var temp = 
        `<div class="th-hunt-progress-content">
          ${closeBtn}
          <b>Badges Collected</b>
          <div class="badges-array">
            ${badgeArray}
          </div>
        </div>`;
        return temp;
    }

    function treasureHuntOncomplete(html){
        var modal = document.createElement("div");
        var overlay = document.createElement("div");
        var pp = document.createElement("div");
        modal.id = "th-complete-modal";
        modal.classList.add("th-modal");
        overlay.classList.add("th-modal-overlay");
        pp.innerHTML = html;
        document.body.appendChild(modal);

        $("#th-complete-modal").append(pp);
        $("#th-complete-modal").after(overlay);
        $("#th-complete-modal img.complete-img").on('load', function(){
            $(this).css({
                'opacity':'1',
                'transform': 'scale(1)',
            });
        });
        
        overlay.onclick = function () {
            var modal = document.getElementById("th-complete-modal");
            modal.remove();
            $(this).remove();
        };
    };

    function initThPassportTab(){
        var lootsProgress = JSON.parse(localStorage.getItem("sg-treasurehunt"));
        var huntedIds = JSON.parse(localStorage.getItem("sg-treasurehunt")) ? lootsProgress.huntedIds : [];
        var badgeArray = [];
        for(var i = 0; i < loots.length; i++){
          var blankBadge = blankLootSrc;
          var badgeSrc = huntedIds.includes(loots[i].id) ? loots[i].imgSrc : blankBadge;
          badgeArray.push(`<img src="${badgeSrc}" width="48px" />`);
        }
        var badgeArray = badgeArray.join("");
        var temp = 
        `<div class="th-hunt-progress-content">
          <div class="badges-array">
            ${badgeArray}
          </div>
        </div>`;
      $(`#${progressPassportId}`).html(temp);
    };
};